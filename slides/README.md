---
title: Rust
verticalSeparator: ^\-\-\-\-$
---

# Rust

---

## Disclaimer

<ul>
<li class="fragment">intro
<li class="fragment">à l'arrache
<li class="fragment">noob
</ul>

---

## Objectifs

<ul>
<li class="fragment">donner envie de s'intéresser à Rust
<li class="fragment">faire découvrir de nouvelles possibilités
<li class="fragment">réutiliser les patterns de Rust
</ul>

---

🦀 ≠ ☢️

<span class="fragment">

`XYZ rust lang`

</span>

---

## 🦀

----

<img style="aspect-ratio: 1 / 1; background-color: white;" src="https://www.rust-lang.org/logos/rust-logo-blk.svg" />

----

🦀 🦐

<div class="r-stack" style="font-family: monospace;">

<span class="fragment current-visible" data-fragment-index="0">

Crustacean

</span>

<span class="fragment current-visible">

C**RUST**acean

</span>

<span class="fragment current-visible">

~~c~~**RUST**acean

</span>

<span class="fragment">

&nbsp;Rustacean

</span>

</div>

<div class="fragment">

Ferris

<img style="max-height: 40vmin;" src="https://www.rustacean.net/assets/rustacean-orig-noshadow.svg" />

</div>

note:
    mascotte officieuse ;
    vite ;

---

## 🧠 Mémoire

----

<table>
    <thead>
        <tr>
            <th>
            <th>Manual Memory Management
            <th>Garbage Collector 🗑️
    <tbody>
        <tr class="fragment" data-fragment-index="0">
            <th>Langages
            <td>C / C++
            <td>JS, Python, Java, Ruby, PHP…
        <tr class="fragment" data-fragment-index="1">
            <th>Complexité 🤯
            <td>Dev 🧑‍💻
            <td>Runtime 🤖
        <tr class="fragment" data-fragment-index="2">
            <th>Problème 😥
            <td>Fuite mémoire 📈
            <td>Performance 📉
        <tr class="fragment" data-fragment-index="2">
            <td>
            <td><code>SEGFAULT</code>
            <td>▶️⏸️🚮▶️🔄
</table>

note:
    rappel fonctionnement de l'allocation mémoire pour les gens qui n'ont jamais fait de `c`

----

```javascript
function f() {
  const x = {};
  const y = {};
  x.a = y; // x references y
  y.b = x; // y references x

  return "azerty";
}

f();
```

----

Contrainte 🆚 Garantie

<span class="fragment">🎭 🪙</span>

----

```rust
fn foo() {
    let pencil = String::from("✏️");

    display_stuff(pencil);

    dbg!(pencil); // 💥
}
```

<div class="fragment">

> Donner, c'est donner ;
> <br />
> reprendre, c'est voler

</div>

----

```rust
fn foo() {
    let pencil = String::from("✏️");

    display_stuff(&pencil);
    //            ^

    dbg!(pencil); // ✅
}
```

<div class="fragment">

> Je te prête mon crayon,
> <br />
> mais il s'appelle "reviens"

</div>

<div class="fragment">

[Ownership / Borrow (empreinter) Checker](https://en.wikipedia.org/wiki/Rust_%28programming_language%29#Ownership_and_lifetimes)

</div>

---

Rust ?

<div class="fragment">

🔴 rouille

</div>

---

## Modélisation

<style>
    .code-no-margin {
        .columns {
            display: flex;
            gap: 1vw;
        }

        pre {
            margin: 0.5em 0;

            code.hljs {
                padding: 0.2em;
            }
        }
    }
</style>

----

<!-- .slide: data-auto-animate -->

```typescript
type Query = {
  isLoading: boolean;
  isSuccess: boolean;
  data?: string;
  hasError: boolean;
  error?: Error;
};
```

```typescript
const query: Query = http.get("https://example.com");
```

```typescript
if (query.isLoading) console.log("is loading");
else if (query.isSuccess) console.log(query.data);
else if (query.hasError) console.error(query.error);
```

<div class="fragment">

```typescript
const query: Query = {
  isLoading: true,
  isSuccess: true,
  hasError: true,
};
```

</div>

----

<!-- .slide: data-auto-animate -->

<div class="code-no-margin">

<div class="columns">

```typescript
type LoadingState = {
  isLoading: true;
  isSuccess: false;
  data?: never;
  hasError: false;
  error?: never;
};
```

```typescript
type ErrorState = {
  isLoading: false;
  isSuccess: false;
  data?: never;
  hasError: true;
  error: Error;
};
```

```typescript
type SuccessState = {
  isLoading: false;
  isSuccess: true;
  data: string;
  hasError: false;
  error?: never;
};
```

</div>

```typescript
type Query = LoadingState | SuccessState | ErrorState;
```

```typescript
const query: Query = http.get("https://example.com");
```

```typescript
if (query.isLoading) console.log("is loading");
else if (query.isSuccess) console.log(query.data);
else if (query.hasError) console.error(query.error);
```

</div>

----

<!-- .slide: data-auto-animate -->

<div class="code-no-margin">

```typescript
enum State {
  LOADING,
  SUCCESS,
  ERROR,
}
```

<div class="columns">

```typescript
type LoadingState = {
  state: State.LOADING;
  data?: never;
  error?: never;
};
```

```typescript
type SuccessState = {
  state: State.SUCCESS;
  data: string;
  error?: never;
};
```

```typescript
type ErrorState = {
  state: State.ERROR;
  data?: never;
  error: Error;
};
```

</div>

```typescript
type Query = LoadingState | SuccessState | ErrorState;
```

```typescript
const query: Query = http.get("https://example.com");
```

```typescript
switch (query.state) {
  case State.LOADING: console.log("is loading"); break;
  case State.SUCCESS: console.log(query.data); break;
  case State.ERROR: console.error(query.error); break;
}
```

</div>

----

> [Easy To Use And Hard To Misuse](http://principles-wiki.net/principles:easy_to_use_and_hard_to_misuse)

----

<!-- .slide: data-auto-animate -->

<div data-id="code-animation-type">

```rust
enum Query {
    IsLoading,
    IsSuccess(String),
    HasError(Error),
}
```

</div>

```rust
let query: Query = http::get("https://example.com");
```

<div data-id="code-animation-pattern-matching">

```rust
match query { // <- pattern matching AKA switch plus puissant
    Query::IsLoading => println!("is loading"),
    Query::IsSuccess(data) => println!("{}", data),
    Query::HasError(error) => eprintln!("{}", error),
};
```

</div>

----

<!-- .slide: data-auto-animate -->

<div data-id="code-animation-type">

```rust
enum Query {
    IsLoading,
    Loaded(Response),
}
enum Response {
    IsSuccess(String),
    HasError(Error),
}
```

</div>

```rust
let query: Query = http::get("https://example.com");
```

<div data-id="code-animation-pattern-matching">

```rust
match query {
    Query::IsLoading => println!("is loading"),
    Query::Loaded(Response::IsSuccess(data)) => println!("{}", data),
    Query::Loaded(Response::HasError(error)) => eprintln!("{}", error),
};
```

</div>

----

```rust
type Query = Option<Result<String, Error>>;
```

----

Compilateur force vérifier tout les cas

<div class="fragment">

dont les cas alternatifs

</div>

----

Pas d'exception

<div class="fragment">

`panic!()`

</div>

----

Verbeux

----

Difficile à faire compiler au début

----

Quand ça compile = ça fonctionne

😌

---

## [Typage](https://en.wikipedia.org/wiki/Comparison_of_programming_languages_by_type_system)

----

<table>
    <thead>
        <tr>
            <th>Rust 🦀
            <th>JavaScript
            <th>TypeScript
            <th>Python 🐍
    <tbody>
        <tr class="fragment" data-fragment-index="0">
            <td>Fort
            <td>Faible
            <td>Fort ?
            <td>Fort
        <tr class="fragment" data-fragment-index="1">
            <td>Static
            <td>Dynamic
            <td>Static
            <td>Dynamic
        <tr class="fragment" data-fragment-index="2">
            <td>Inferred <code>*</code>
            <td>
            <td>Inferred
            <td>
        <tr class="fragment" data-fragment-index="4">
            <td>Nominal
            <td>Duck 🦆
            <td>Structural / Duck 🦆
            <td>Duck 🦆
</table>

<div class="r-stack">

<div class="fragment current-visible" data-fragment-index="3">

`*` sauf entrée / sortie des fonctions

</div>

<div class="fragment current-visible" data-fragment-index="5">

> If it walks like a duck and it quacks like a duck, then it must be a duck

</div>

</div>

---

## Tooling

<small>

<table>
    <thead>
        <tr>
            <th>Langage
            <th>TypeScript
            <th>Rust 🦀
            <th>Python 🐍
    <tbody>
        <tr class="fragment">
            <th>Version Manager
            <td>FNM, NVM, n…
            <td><a href="https://rustup.rs">Rustup</a>
            <td>🤷 pyenv ?
        <tr class="fragment">
            <th>Plateforme
            <td>NodeJS, Deno, bun
            <td>🚫 binaire
            <td>CPython, PyPy…
        <tr class="fragment">
            <th>Compiler
            <td><code>tsc</code>
            <td>Cargo (via <code>rustc</code>)
            <td>🚫 interprété
        <tr class="fragment">
            <th>Dependencies Manager
            <td>Yarn, NPM…
            <td>Cargo
            <td>Poetry
        <tr class="fragment">
            <th>Formater
            <td>Prettier
            <td>Cargo (via <code>rustfmt</code>)
            <td>Black, isort
        <tr class="fragment">
            <th>Linter
            <td>ESLint
            <td>Cargo, Clippy
            <td>Flake8, mypy
        <tr class="fragment">
            <th>Test Framework
            <td>Jest, Vitest…
            <td>✅ dans le langage
            <td>pytest
</table>

</small>

note:
    Meilleur tooling que j'ai vu ;
    Regarder votre colonne ;
    Jython, IronPython ;

----

Compilateur <span class="fragment" data-fragment-index="0">aidant 🤖❤️🤗</span>

> Il y a une erreur ici,
> <br />
> <span class="fragment" data-fragment-index="0">pour corriger,</span>
> <br />
> <span class="fragment" data-fragment-index="0">tu peux essayer de faire ça</span>

----

Test unitaire collocaliser dans les fichiers sources

---

https://github.com/rust-unofficial/awesome-rust#readme

https://roogle.hkmatsumoto.com/

https://programming-idioms.org/

https://rosettacode.org/wiki/Category:Rust

---

## Fibonacci

https://the-algorithms.com/fr/algorithm/fibonacci-numbers?lang=rust

https://rosettacode.org/wiki/Fibonacci_sequence#Rust

https://programming-idioms.org/idiom/301/recursive-fibonacci-sequence

---

Questions ?

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/initiation_rust">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/initiation_rust?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
